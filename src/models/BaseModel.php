<?php

namespace Src\models;

use Src\helpers\Helpers;

class BaseModel {

  protected $entity;
  function __construct(String $entity) {
    $this->entity = $entity;
	}

  function getInstanceModel() {
    return (new Helpers())->getJson($this->entity);
  }

}