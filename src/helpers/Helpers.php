<?php

namespace Src\helpers;

class Helpers {
	function putJson($users, $entity) {
		// file_put_contents(dirname(__DIR__) . '/../scripts/'.$entity.'.json', json_encode($users, JSON_PRETTY_PRINT));
		file_put_contents('/var/www/html/scripts/' . $entity . '.json', json_encode($users, JSON_PRETTY_PRINT));

	}
	public static function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}

	public static function getJson($entity) {
		$string = file_get_contents(realpath(dirname(__DIR__)) .'/../scripts/'. $entity . '.json');
		return json_decode($string, true);
	}
}